var bcrypt = require('bcrypt');
var db = require('../db/db');
var Sequelize = require('sequelize');
var Review = require('../Models/review')(db.db, Sequelize);
var agoraErrors = require('../utilities/agoraErrors');
var express = require('express');
var router = express.Router();

function handleError(request ,response){
    response.status(response.locals.error.error.statusCode).json(response.locals.error);
}

function addReview(request, response, next) {
    Review.create({
        reviewedUserId: request.body.reviewedUserId,
        reviewDescription: request.body.reviewDescription,
        reviewerUserId: request.body.reviewerUserId,
        rating: request.body.rating
    }).then(function (review) {
        //add to event sourcing
        return response.json({
            review: review
        })
    }).catch(function (error) {
        console.log(error);
        response.locals.error = agoraErrors.addReview;
        next();
    })
}

router.post('/', addReview, handleError);

module.exports = router;
/**
 * Created by justin on 8/22/17.
 */
