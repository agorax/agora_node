var bcrypt = require('bcrypt');
var db = require('../db/db');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var Sequelize = require('sequelize');
var User = require('../Models/user')(db.db, Sequelize);
var Item = require('../Models/item')(db.db, Sequelize);
var Review = require('../Models/review')(db.db, Sequelize);
var agoraErrors = require('../utilities/agoraErrors');
var Parse = require('parse/node').Parse;
var parseConfiguration = require("../configurations/parseConfiguration");
var express = require('express');
var app = express();
var router = express.Router();

function handleError(request ,response){
    if (response.locals.error) {
        response.status(response.locals.error.error.statusCode).json(response.locals.error);
    } else {
        response.status(202).send({
        })
    }

}

function updateProfile(request, response, next) {
    User.findOne({
        where: {
            'userId': request.param(('userId'))
        }
    }).then(function (agoraUser) {
        if (!agoraUser) {
            response.locals.error = agoraErrors.profileUpdate;
            return next();
        }
        if (request.param('name')) {
            updateName(agoraUser, request.param('name'), response, next)
        } else if (request.param('bio')) {
            updateBio(agoraUser, request.param('bio'), response, next)
        } else if (request.param('location')) {
            updateLocation(agoraUser, request.param('location'), response, next)
        } else {
            response.locals.error = agoraErrors.profileUpdate;
            return next();
        }
    })
}

function updateName(agoraUser, name, response, next) {
    if(agoraUser){
        agoraUser.updateAttributes({
            name:name
        }).then(function (data1) {
            next();
        }).catch(function (error) {
            response.locals.error = agoraErrors.profileUpdate;
            next();
        })
    }
}

function updateBio(agoraUser, bio, response, next) {
    if(agoraUser){
        agoraUser.updateAttributes({
            bio:bio
        }).then(function (data1) {
            next();
        }).catch(function (error) {
            response.locals.error = agoraErrors.profileUpdate;
            next();
        })
    } else {
        response.locals.error = agoraErrors.profileUpdate;
        next();
    }
}

function updateLocation(agoraUser, location, response, next) {
    if(agoraUser){
        agoraUser.updateAttributes({
            location:location
        }).then(function (data1) {
            next();
        }).catch(function (error) {
            response.locals.error = agoraErrors.profileUpdate;
            next();
        })
    } else {
        response.locals.error = agoraErrors.profileUpdate;
        next();
    }
}

function zeroBadge(request, response) {
  User.findOne({
    where: {
      'userId':request.params.userId
    }
  }).then(function(user) {
    zeroBadgeForUser(user, response)
  }).catch(function(error) {
    console.log(error);
  })
}

function zeroBadgeForUser(user, response) {
  user.updateAttributes({
    badgeCount: 0
  }).then(function(){
    response.status(202).send();
  }).catch(function(error) {
    console.log(error);
  })
}

/**
 * handling domain/login post request
 * returns a token if authentication successful
 */
router.post('/', updateProfile, handleError);

router.post('/zeroBadge/:userId', zeroBadge);

module.exports = router;
