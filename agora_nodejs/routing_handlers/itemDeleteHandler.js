var agoraErrors = require('../utilities/agoraErrors');
var Sequelize = require('sequelize');
var db = require('../db/db');
var express = require('express');
var app = express();
var router = express.Router();
var Message = require('../Models/message')(db.db, Sequelize);
var Conversation = require('../Models/conversation')(db.db, Sequelize);
var User = require('../Models/user')(db.db, Sequelize);
var Item = require('../Models/item')(db.db, Sequelize);
var PushAgent = require('../utilities/apnsAgent');

var apnsAgent = PushAgent.agent;

function handleError(request ,response){
    if (response.locals.error) {
        response.status(response.locals.error.error.statusCode).json(response.locals.error);
    } else {
        response.status(100).send({
        })
    }
}

router.post('/:itemId', findAndDeleteConversations, handleError);


function deleteItem(request,response, t, recipientIds, next) {
    return Item.destroy({
        where: {
          "itemId": request.params.itemId
        }
      }, {transaction: t}).then(function(result) {
          response.json({ itemId: request.params.itemId })
          recipientIds.forEach(function (recipientId) {
            getUserForIdAndSendDeleteItemPush(recipientId, request.body.senderDisplayName, request.body.itemName, request.params.itemId)
          })
      }).catch(function(error) {
          console.log(error);
          response.locals.error = agoraErrors.itemDelete;
          next();
      })
}



function findAndDeleteConversations(request, response, next) {
  var recipientIds = []
  db.db.transaction(function(t) {
    return Conversation.findAll({
      where: {
        "itemId": request.params.itemId
      }
    }, {transaction: t}).then(function(conversations) {
        return conversations.forEach(function (conversation) {
          recipientIds.push(conversation.buyerId);
          return deleteConversation(request, response, t, conversation, next)
        })
    }, {transaction: t}).then(function() {
      return deleteItem(request, response, t, recipientIds, next)
    })
  })
}

function deleteConversation(request, response, t, conversation, next) {
  return Message.destroy({
    where: {
      "conversationId": conversation.conversationId
    }
  },{transaction: t}).then(function() {
    return Conversation.destroy({
      where: {
        "conversationId": conversation.conversationId
      }
    },{transaction: t}).then(function(result) {

    }).catch(function(error) {
        console.log(error);
        response.locals.error = agoraErrors.conversationDeletion
        next();
    })
  })
}

var getUserForIdAndSendDeleteItemPush = function(userId, senderDisplayName, itemName, itemId) {
  User.findOne({
      where: {
          'userId': userId
      }
  }).then(function(user) {
    var message = senderDisplayName + " has deleted " + itemName
    PushAgent.sendItemPushWithAlertAndType(user.apnsToken, user.badgeCount+1, message, "itemDeleted", itemId)
    incrementBadgeCountForUser(user, user.badgeCount+1)
  }).catch(function(error) {
    console.log(error);
  })
}

var incrementBadgeCountForUser = function(user, badgeCount) {
  user.updateAttributes({
    badgeCount: badgeCount
  }).catch(function(error) {
    console.log(error);
  })
}

module.exports = router;
