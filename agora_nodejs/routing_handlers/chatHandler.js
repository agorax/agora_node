var agoraErrors = require('../utilities/agoraErrors');
var Sequelize = require('sequelize');
var db = require('../db/db');
var express = require('express');
var app = express();
var router = express.Router();
var Message = require('../Models/message')(db.db, Sequelize);
var Conversation = require('../Models/conversation')(db.db, Sequelize);
var User = require('../Models/user')(db.db, Sequelize);
var PushAgent = require('../utilities/apnsAgent');

var apnsAgent = PushAgent.agent;
var httpServer;
var log = console.log.bind(this);

function handleError(request ,response){
    response.status(response.locals.error.error.statusCode).json(response.locals.error);
}

router.get('/:user', getConversationsForUser, handleError);
router.get('/:conversation', getConversation, handleError);
router.get('/:conversation/messages', getMessagesForConversation, handleError);
router.post('/:conversationId', deleteConversation, handleError);
router.post('/zeroUnreadNumber/:conversationId', zeroUnreadNumber);

function getConversationsForUser(request, response, next) {
  var userId = request.param('user')
  Conversation.findAll({
    where: {$or:{'sellerId':userId, 'buyerId': userId}}
  }).then(function(conversations){
    response.status(202).send({
      conversations: conversations
    })
  }).catch(function(error){
    response.locals.error = agoraErrors.noConversations;
    next();
  })
}

function getConversation(request, response, next) {
  var conversationId = request.params('conversation')
  Conversation.findOne({
    where: {
      'conversationId' : conversationId
    }
  }).then(function(conversation){
    response.status(202).send({
      conversation: conversation
    })
  }).catch(function(error){
    response.locals.error = agoraErrors.noConversation
    next();
  })
}

function getMessagesForConversation(request, response, next) {
  let conversationId = request.param('conversation')
  Message.findAll({
    where: {
      "conversationId": conversationId
    }
  }).then(function(messages){
    if (messages.length == 0) {
      response.locals.error = agoraErrors.noMessages
      next();
    } else {
        response.status(202).send({
        messages: messages
      })
    }
  }).catch(function(error){
    response.locals.error = agoraErrors.noMessages
    next();
  })
}

function deleteConversation(request, response, next) {
  db.db.transaction(function(t) {
    return Message.destroy({
      where: {
        "conversationId": request.params.conversationId
      }
    },{transaction: t}).then(function() {
      return Conversation.destroy({
        where: {
          "conversationId": request.params.conversationId
        }
      },{transaction: t})
    })
  }).then(function(result) {
    response.status(202).send()
    getUserForIdAndSendDeleteConversationPush(request.body.recipientId, request.body.senderDisplayName, request.body.itemName, request.params.conversationId)
  }).catch(function(error) {
    console.log(error);
    response.locals.error = agoraErrors.conversationDeletion
    next();
  })
}

function zeroUnreadNumber(request, response) {
  Conversation.findOne({
    where: {
      'conversationId': request.params.conversationId
    }
  }).then(function(conversation) {
    zeroUnreadNumberForConversation(conversation,request.body.isSeller,response)
  }).catch(function(error) {
    console.log(error);
  })
}

function zeroUnreadNumberForConversation(conversation, isSeller, response) {
  if (isSeller) {
    conversation.updateAttributes({
      sellerUnreadNumber: 0
    }).then(function(){
      response.status(202).send();
    }).catch(function(error) {
      console.log(error);
    })
  } else {
    conversation.updateAttributes({
      buyerUnreadNumber: 0
    }).then(function(){
      response.status(202).send();
    }).catch(function(error) {
      console.log(error);
    })
  }
}




// These two should be model methods
var getUserForId = function(userId) {
  User.findOne({
    where: {
        'userId': userId
    }
  }).then(function(user) {
    return user
  }).catch(function(error) {
    console.log(error);
  })
}

var getItemForId = function(itemId) {
  Item.findOne({
    where: {
        'itemId': itemId
    }
  }).then(function(item) {
    return item
  }).catch(function(error) {
    console.log(error);
  })
}

var getUserForIdAndSendDeleteConversationPush = function(userId, senderDisplayName, itemName, conversationId) {
  User.findOne({
      where: {
          'userId': userId
      }
  }).then(function(user) {
    var message = senderDisplayName + " has ended the conversation about " + itemName
    PushAgent.sendPushWithAlertAndType(user.apnsToken, user.badgeCount+1, message, "conversationDeleted", conversationId)
    incrementBadgeCountForUser(user, badgeCount)
  }).catch(function(error) {
    console.log(error);
  })
}

// Should be a model method as well
var incrementBadgeCountForUser = function(user, badgeCount) {
  user.updateAttributes({
    badgeCount: badgeCount
  }).catch(function(error) {
    console.log(error);
  })
}

module.exports = router;
