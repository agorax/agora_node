var db = require('../db/db');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var Sequelize = require('sequelize');
var User = require('../Models/user')(db.db, Sequelize);
var express     = require('express');
var emailConfigs = require('../configurations/emailConfiguration')
var app = express();
var router = express.Router();
var agoraErrors = require('../utilities/agoraErrors');
app.set('agoraEmailSecretKey', emailConfigs.secret);


/**
 * Receives email verification link request
 * @param request
 * @param response
 */
var verifyEmail = function(request, response) {
  var token = request.query["token"]
  if (token) {
    // verifies token
    verifyToken(request, response, token)
  } else {
    return response.status(403).send({
        success: false,
        message: 'No token provided.'
    });
  }
}

/**
 * Verifies the token from the email link
 * @param request
 * @param response
 * @param token
 */
var verifyToken = function(request, response, token) {
  jwt.verify(token, app.get('agoraEmailSecretKey'), function(err, decoded) {
    if (err) {
      return response.json({ success: false, message: 'Failed to authenticate token.' });
    } else {
      request.decoded = decoded;
      User.findOne({where: {emailVerificationData: token}}).then(function(user) {
        user.updateAttributes({
          emailVerified: true,
          emailVerificationData: null
        }).then(function() {
          response.status(202).send('<p>You have been verified!</p><p>You may now enter AgoraExchange.</p>')
        });
      })
    }
  });
}


router.get('/', verifyEmail);

module.exports = router;
