var bcrypt = require('bcrypt');
var db = require('../db/db');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var Sequelize = require('sequelize');
var User = require('../Models/user')(db.db, Sequelize);
var Item = require('../Models/item')(db.db, Sequelize);
var Review = require('../Models/review')(db.db, Sequelize);
var agoraErrors = require('../utilities/agoraErrors');
var Parse = require('parse/node').Parse;
var parseConfiguration = require("../configurations/parseConfiguration");
var express = require('express');
var app = express();
var router = express.Router();

function handleError(request ,response){
    if (response.locals.error) {
        response.status(response.locals.error.error.statusCode).json(response.locals.error);
    } else {
        response.status(100).send({
        })
    }
}

function updateItem(request, response, next) {
    Item.findOne({
        where: {
            'itemId': request.param(('itemId'))
        }
    }).then(function (item) {
        if (!item) {
            response.locals.error = agoraErrors.itemUpdate;
            return next();
        }
        if (request.param('price')) {
            updatePrice(item, request.param('price'), response, next)
        } else if (request.param('itemDescription')) {
            updateDescription(item, request.param('itemDescription'), response, next)
        } else {
            response.locals.error = agoraErrors.itemUpdate;
            return next();
        }
    })
}

function updatePrice(item, price, response, next) {
    if(item){
        item.updateAttributes({
            price:price
        }).then(function (data1) {
            next();
        }).catch(function (error) {
            response.locals.error = agoraErrors.itemUpdate;
            next();
        })
    } else {
        response.locals.error = agoraErrors.itemUpdate;
        next();
    }
}

function updateDescription(item, description, response, next) {
    if(item){
        item.updateAttributes({
            itemDescription:description
        }).then(function (data1) {
            next();
        }).catch(function (error) {
            response.locals.error = agoraErrors.itemUpdate;
            next();
        })
    } else {
        response.locals.error = agoraErrors.itemUpdate;
        next();
    }
}

/**
 * handling domain/login post request
 * returns a token if authentication successful
 */
router.post('/', updateItem, handleError);

module.exports = router;