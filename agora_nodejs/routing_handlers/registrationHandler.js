var bcrypt = require('bcrypt');
var db = require('../db/db');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var Sequelize = require('sequelize');
var User = require('../Models/user')(db.db, Sequelize);
var express = require('express');
var emailConfigs = require('../configurations/emailConfiguration');
var agoraErrors = require('../utilities/agoraErrors');
var app = express();
var router = express.Router();
app.set('agoraEmailSecretKey', emailConfigs.secret);

function handleError(request ,response){
    if (response.locals.error) response.status(response.locals.error.error.statusCode).json(response.locals.error);
}

const emailSuffixForSchool = {
    'Case Western': '@case.edu',
    'Baldwin Wallace': '@mail.bw.edu',
    'Ohio University': '@ohio.edu'
}
/**
 * Sends request object to validator and either createServer
 * a new user or sends error based on validation
 * @param request
 * @param response
 */
function beginRegistration(request, response, next) {
    if (response.locals.error) { next(); return; }
    createNewUser(request, response, next);
}

function validateRegistrationParameters(request, result, next) {
    var email = request.param('email');
    var password = request.param('password');
    var school = request.param('school');
    if (!validateEmail(email, school)) {
        result.locals.error = agoraErrors.notSchoolEmail(school);
    }
    else if (!validatePasswordLength(password)) {
        result.locals.error = agoraErrors.passwordLength;
    }
    next();
}

router.post('/', validateRegistrationParameters, beginRegistration, handleError);

validatePasswordLength = function(password) {
    return password.length >= 8;
}

validateEmail = function (email, school) {
    var correctEmailSuffix = emailSuffixForSchool[school];
    if (email.length <= correctEmailSuffix) return false;
    var emailSuffix = email.substring(email.length - correctEmailSuffix.length, email.length);
    return emailSuffix === correctEmailSuffix;
}


/**
 * Creates a new user with request object and generated salt
 * @param request
 * @param response
 */
createNewUser = function (request, response, next) {
    var salt = bcrypt.genSaltSync(10)
    var hashedPassword = bcrypt.hashSync(request.body.password, salt)
    User.create({
        email: request.body.email,
        password: hashedPassword,
        salt: salt,
        school: request.body.school
    }).then(function (user) {
        sendVerificationEmail(user, response, next);
    }).catch(function (error) {
        response.locals.error = agoraErrors.userAlreadyExists;
        next();
    })
}

// EMAIL VERIFICATION

/**
 * Sends an email to the user with a verification link
 * @param User
 */
var sendVerificationEmail = function (user, response, next) {
    var token = emailToken(user);
    emailConfigs.transporter.sendMail(emailConfigs.EMAIL_OPTIONS(user, token), function (error, info) {
        if (error) {
            response.locals.error = agoraErrors.noEmailFound;
            next();
        } else {
            response.status(100).send({message: "Successful Registration"});
        }
    });
}

/**
 * Create verification token and set token on the emailVerficationData
 * @param user
 */
var emailToken = function (user) {
    var token = jwt.sign({user: user}, app.get('agoraEmailSecretKey'), {
        expiresIn: '6h'// expires in 6 hours
    })
    user.updateAttributes({emailVerificationData: token});
    return token
}

module.exports = router;
