var bcrypt = require('bcrypt');
var db = require('../db/db');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var Sequelize = require('sequelize');
var User = require('../Models/user')(db.db, Sequelize);
var agoraErrors = require('../utilities/agoraErrors');
var Parse = require('parse/node').Parse;
var parseConfiguration = require("../configurations/parseConfiguration");
var express = require('express');
var app = express();
var router = express.Router();

app.set('agoraAPISecretKey', db.secret);

Parse.initialize(parseConfiguration.PARSE_APPLICATION_ID, parseConfiguration.PARSE_JAVASCRIPT_API_KEY);
Parse.serverURL = 'https://parseapi.back4app.com'

function handleError(request ,response){
  if (response.locals.error) {
    response.status(response.locals.error.error.statusCode).json(response.locals.error);
  }
}

/**
 * sends back the user and the associated token for that user.
 * @param request
 * @param response
 */
function issueToken(request, response, next) {
    if (response.locals.error != null) return next();
    var token = jwt.sign({user: request.user}, app.get('agoraAPISecretKey'), {
        expiresIn: '14d'// expires in 2 weeks
    });
    response.status(202).send({
        token: token,
        user: response.locals.user
    })
}

function authenticateAgoraUser(request, response, next) {
    fetchUserFromAgoraDBAndAuthenticate(request.param('email'), request.param('password'), request.param('apnsToken'), response, next);
}

/**
 * handling domain/login post request
 * returns a token if authentication successful
 */
router.post('/', authenticateAgoraUser, issueToken, handleError);

/**
 * fetch a user with the specified email from the database and proceed with authentication via callback
 * @param email: email to specify user to fetch
 * @param password: entered password
 * @param done: done callback used in authentication callback
 */
fetchUserFromAgoraDBAndAuthenticate = function (email, password, apnsToken, response, next) {
    User.findOne({
        where: {
            'email': email
        }
    }).then(function (agoraUser) {
        authenticateWithFetchedUser(agoraUser, password, apnsToken, response, next);
    })
};

/**
 * authenticate the fetched user
 * @param agoraUser: the fetched user
 * @param done: callback to use with passport once user has been authenticated (or rejected)
 */
authenticateWithFetchedUser = function (agoraUser, passwordEntered, apnsToken, response, next) {
    if (verifyUserExists(agoraUser, response, next) == false) return;
    (agoraUser.password == null) ? updatePasswordFromParse(agoraUser.email, passwordEntered, agoraUser, apnsToken, response, next) : authenticateUser(agoraUser, agoraUser.password, passwordEntered, apnsToken,response, next);
}

/**
 * if user does not exist in our DB, call done with error message
 * @param agoraUser: user to be verified
 * @param done: callback to use with passport once user has been authenticated (or rejected)
 * @returns {boolean}: true if user exists in DB
 */
verifyUserExists = function (agoraUser, response, next) {
    if (agoraUser == null) {
        response.locals.error = agoraErrors.noUserFound;
        next();
    }
    console.log("not null");
    return agoraUser != null;
}

/**
 * update the user's password and authenticate them once the password is updated.
 * @param agoraUser: agora user who's password will be updated
 * @param password: the new password
 * @param done: callback to use with passport once user has been authenticated (or rejected)
 */
updateUserPasswordAndAuthenticate = function (agoraUser, password, apnsToken, response, next) {
    var salt = bcrypt.genSaltSync(10)
    var hashedPassword = bcrypt.hashSync(password, salt)
    agoraUser.updateAttributes({
        password: hashedPassword,
        salt: salt,
        apnsToken: apnsToken
    }).then(function () {
        console.log("update success");
        response.locals.user = agoraUser;
        next();
    }).catch(function (error) {
        console.log("update failed: " + error);
        response.locals.error = agoraErrors.internalError;
        next();
    })
}

/**
 * login with parse to determine correct password. Then update the user's password.
 * @param email: user's email
 * @param password: user's password
 * @param agoraUser: user to be updated
 * @param done: callback to use with passport once user has been authenticated (or rejected)
 */
updatePasswordFromParse = function (email, password, agoraUser, apnsToken, response, next) {
    Parse.User.logIn(email, password, {
        success: function (parseUser) {
          console.log("Parse success");
            updateUserPasswordAndAuthenticate(agoraUser, password, apnsToken, response, next);
        },
        error: function (error) {
          console.log("parse error");
            response.locals.error = agoraErrors.internalError;
            next();
        }
    })
}

/**
 * authenticates user if hashedpassword and password entered are the same
 * @param agoraUser: user to be authenticated
 * @param hashedPassword: hashed password from database
 * @param passwordEntered: password entered on frontend
 * @param done: callback to use with passport once user has been authenticated (or rejected)
 * @returns {*}
 */
authenticateUser = function (agoraUser, hashedPassword, passwordEntered, apnsToken, response, next) {
    if (!agoraUser.emailVerified) {response.locals.error = agoraErrors.verifyEmail; next();}
    var correctPassword = bcrypt.compareSync(passwordEntered, hashedPassword);
    if (correctPassword) {
      response.locals.user = agoraUser;
      if(apnsToken) {
        updateDeviceToken(agoraUser, apnsToken, next, response);
      } else {
        next();
      }
    } else {
      response.locals.error = agoraErrors.wrongPassword;
      next();
    }
}

updateDeviceToken = function(agoraUser, apnsToken, next, response) {
    agoraUser.updateAttributes({
        apnsToken: apnsToken
    }).then(function () {
        next();
    }).catch(function (error) {
        response.locals.error = agoraErrors.internalError;
        next();
    })
}

module.exports = router;
