var bcrypt = require('bcrypt');
var db = require('../db/db');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var Sequelize = require('sequelize');
var User = require('../Models/user')(db.db, Sequelize);
var Item = require('../Models/item')(db.db, Sequelize);
var Review = require('../Models/review')(db.db, Sequelize);
var Conversation = require('../Models/conversation')(db.db, Sequelize);
var agoraErrors = require('../utilities/agoraErrors');
var Parse = require('parse/node').Parse;
var parseConfiguration = require("../configurations/parseConfiguration");
var express = require('express');
var app = express();
var router = express.Router();

function handleError(request ,response){
    response.status(response.locals.error.error.statusCode).json(response.locals.error);
}

function nextIfReady(response, next) {
    if (response.locals.users && response.locals.items && response.locals.reviews && response.locals.conversations) next();
}

function fetchMarketplaceData(request, response, next) {
    User.findAll({
        where: {
            school: 'Case Western'
        }
    }).then(function (users) {
        response.locals.users = users;
        nextIfReady(response, next);
    })
    Item.findAll({
        where: {
            school: 'Case Western'
        }
    }).then(function (items) {
        response.locals.items = items;
        nextIfReady(response, next);
    })
    Review.findAll({
    }).then(function (reviews) {
        response.locals.reviews = reviews;
        nextIfReady(response, next);
    })
    Conversation.findAll({
    }).then(function (conversations) {
        response.locals.conversations = conversations;
        nextIfReady(response, next);
    })
}

function formatDataAndSendAsJSON(request, response, next) {
    if (response.locals.error) {next(); return;}
    return response.json({
        users: response.locals.users,
        items: response.locals.items,
        reviews: response.locals.reviews,
        conversations: response.locals.conversations
    })
}

/**
 * handling domain/login post request
 * returns a token if authentication successful
 */
router.post('/', fetchMarketplaceData, formatDataAndSendAsJSON, handleError);

module.exports = router;
