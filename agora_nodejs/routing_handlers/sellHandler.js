var bcrypt = require('bcrypt');
var db = require('../db/db');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var Sequelize = require('sequelize');
var User = require('../Models/user')(db.db, Sequelize);
var Item = require('../Models/item')(db.db, Sequelize);
var Textbook = require('../Models/item')(db.db, Sequelize);
var Review = require('../Models/item')(db.db, Sequelize);
var agoraErrors = require('../utilities/agoraErrors');
var Parse = require('parse/node').Parse;
var parseConfiguration = require("../configurations/parseConfiguration");
var express = require('express');
var app = express();
var router = express.Router();

function handleError(request ,response){
    response.status(response.locals.error.error.statusCode).json(response.locals.error);
}

function addItemToDatabase(request, response, next) {
    Item.create({
        name: request.body.name,
        userId: request.body.userId,
        price: request.body.price,
        itemDescription: request.body.itemDescription,
        category: request.body.category,
        school: request.body.school,
        author: request.body.author,
        isbn: request.body.isbn,
        edition: request.body.edition,
        numberOfPictures: request.body.numberOfPictures
    }).then(function (item) {
        //add to event sourcing
        return response.json({
            itemId: item.itemId
        })
    }).catch(function (error) {
      console.log(error);
        response.locals.error = agoraErrors.itemUpdate;
        next();
    })
}

router.post('/item', addItemToDatabase, handleError);

module.exports = router;
