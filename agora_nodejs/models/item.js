/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('item', {
    itemId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'userId'
      }
    },
    price: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    school: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    itemDescription: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    category: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
      isbn: {
        type: DataTypes.INTEGER(13),
          allowNull: true
      },
      author: {
          type: DataTypes.STRING(45),
          allowNull: true
      },
      edition: {
          type: DataTypes.INTEGER(2),
          allowNull: true
      },
      numberOfPictures: {
        type: DataTypes.INTEGER(11)
      }
  }, {
    tableName: 'item'
  });
};
