/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('conversation', {
    conversationId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    itemId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'item',
        key: 'itemId'
      }
    },
    sellerId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'userId'
      }
    },
    buyerId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'userId'
      }
    },
    room: {
      type: DataTypes.STRING(45),
      allowNull: false,
      unique: true
    },
    lastMessage: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    sellerUnreadNumber: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      default: sequelize.literal('0')
    },
    buyerUnreadNumber: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      default: sequelize.literal('0')
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'conversation'
  });
};
