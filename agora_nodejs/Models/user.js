/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user', {
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    email: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    emailVerified: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    bio: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    numRatings: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    rating: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    school: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    location: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    pictureURL: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    salt: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    emailVerificationData: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    apnsToken: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    badgeCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: 0
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    tableName: 'user'
  });
};
