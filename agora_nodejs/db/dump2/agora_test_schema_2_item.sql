-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: agora_test_schema_2
-- ------------------------------------------------------
-- Server version	5.7.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `itemId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `school` varchar(45) NOT NULL,
  `itemDescription` longtext,
  `category` varchar(45) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isbn` bigint(20) DEFAULT NULL,
  `edition` int(2) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`itemId`),
  KEY `fk_item_userId_idx` (`userId`),
  CONSTRAINT `fk_item_userId` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (1,235,100,'CHEM 111 & 145 book','Case Western','0','Textbooks','2017-08-02 22:55:40','2017-08-02 22:55:40',NULL,NULL,NULL),(2,235,100,'MATH 121/122/223','Case Western','0','Textbooks','2017-08-02 22:55:40','2017-08-02 22:55:40',NULL,NULL,NULL),(3,235,25,'Creativity SAGES','Case Western','0','Textbooks','2017-08-02 22:55:40','2017-08-02 22:55:40',NULL,NULL,NULL),(4,235,75,'ENGR 200 (Statics)','Case Western','0','Textbooks','2017-08-02 22:55:40','2017-08-02 22:55:40',NULL,NULL,NULL),(5,235,60,'Creativity SAGES','Case Western','0','Textbooks','2017-08-02 22:55:40','2017-08-02 22:55:40',NULL,NULL,NULL),(6,235,80,'PHYS 122/122','Case Western','0','Textbooks','2017-08-02 22:55:40','2017-08-02 22:55:40',NULL,NULL,NULL),(7,195,35,'Chem+Bio books','Case Western','0','Textbooks','2017-08-02 22:55:40','2017-08-02 22:55:40',NULL,NULL,NULL),(8,195,5,'SAGES book','Case Western','0','Textbooks','2017-08-02 22:55:40','2017-08-02 22:55:40',NULL,NULL,NULL),(9,147,0,'t','Case Western','','Housing','2017-08-21 23:10:55','2017-08-21 23:10:55',NULL,NULL,NULL),(10,147,0,'t','Case Western','','Textbooks','2017-08-21 23:17:13','2017-08-21 23:17:13',NULL,NULL,NULL),(11,147,0,'t','Case Western','','Appliances','2017-08-21 23:28:04','2017-08-21 23:28:04',NULL,NULL,NULL),(12,147,0,'textbook','Case Western','','Textbooks','2017-08-21 23:46:40','2017-08-21 23:46:40',-15948,24,''),(13,147,0,'testinggg','Case Western','','Textbooks','2017-08-21 23:54:59','2017-08-21 23:54:59',1111222233334,1,'John Steinbeck');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-21 19:14:57
