-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: agora-db-instance.ci2jjl1w50ap.us-east-2.rds.amazonaws.com    Database: AgoraDB
-- ------------------------------------------------------
-- Server version	5.6.29-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `reviewerUserId` int(11) NOT NULL,
  `reviewedUserId` int(11) NOT NULL,
  `rating` int(1) NOT NULL,
  `reviewDescription` longtext,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`reviewerUserId`,`reviewedUserId`),
  KEY `review_fk_reviewedUserId_idx` (`reviewedUserId`),
  CONSTRAINT `review_fk_reviewedUserId` FOREIGN KEY (`reviewedUserId`) REFERENCES `user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `review_fk_reviewerUserId` FOREIGN KEY (`reviewerUserId`) REFERENCES `user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (38,139,1,'Tried to buy ramen, she reaked off cugarettes and didnt have any ramen!','2017-08-02 22:56:52','2017-08-02 22:56:52'),(38,170,5,'Good fuckin kid','2017-08-02 22:56:52','2017-08-02 22:56:52'),(38,198,1,'Ripped me iff ','2017-08-02 22:56:52','2017-08-02 22:56:52'),(147,27,5,'Awesome seller. I bought my Chemistry textbook from her, and it was in excellent condition. She was very prompt and very accomadating throughout.','2017-08-02 22:56:52','2017-08-02 22:56:52'),(147,86,5,'Great seller. Better guy.','2017-08-02 22:56:52','2017-08-02 22:56:52'),(147,162,5,'I bought my calculus textbook from Sean. He was very reponsive and the textbook was in excellent condition.','2017-08-02 22:56:52','2017-08-02 22:56:52'),(147,179,1,'','2017-08-02 22:56:52','2017-08-02 22:56:52'),(147,198,4,'Cool guy.','2017-08-02 22:56:52','2017-08-02 22:56:52'),(158,27,3,'She suxxxxxx','2017-08-02 22:56:52','2017-08-02 22:56:52'),(179,38,5,'','2017-08-02 22:56:52','2017-08-02 22:56:52'),(179,101,1,'Made racially questionable comments to me. Very off-putting and creepy boy','2017-08-02 22:56:52','2017-08-02 22:56:52'),(179,147,1,'3\\10','2017-08-02 22:56:52','2017-08-02 22:56:52'),(179,198,2,'Fuck this jabroni','2017-08-02 22:56:52','2017-08-02 22:56:52'),(198,27,5,'Get seller (and cute) ????','2017-08-02 22:56:52','2017-08-02 22:56:52'),(198,38,5,'Stellar guy.','2017-08-02 22:56:52','2017-08-02 22:56:52'),(198,86,5,'Awesome dude ','2017-08-02 22:56:52','2017-08-02 22:56:52'),(198,104,5,'Very timely seller :)','2017-08-02 22:56:52','2017-08-02 22:56:52'),(198,110,5,'A fine young man ','2017-08-02 22:56:52','2017-08-02 22:56:52'),(198,144,5,'','2017-08-02 22:56:52','2017-08-02 22:56:52'),(198,147,5,'','2017-08-02 22:56:52','2017-08-02 22:56:52'),(198,158,5,'','2017-08-02 22:56:52','2017-08-02 22:56:52'),(198,162,5,'','2017-08-02 22:56:52','2017-08-02 22:56:52'),(198,179,1,'Terrible ','2017-08-02 22:56:52','2017-08-02 22:56:52'),(198,197,5,'','2017-08-02 22:56:52','2017-08-02 22:56:52');
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-21 11:48:06
