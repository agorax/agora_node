var agoraErrors = require('./agoraErrors');
var Sequelize = require('sequelize');
var db = require('../db/db');
var Message = require('../Models/message')(db.db, Sequelize);
var Conversation = require('../Models/conversation')(db.db, Sequelize);
var User = require('../Models/user')(db.db, Sequelize);
var Item = require('../Models/item')(db.db, Sequelize);
var PushAgent = require('./apnsAgent');

var apnsAgent = PushAgent.agent;
var httpServer;
var sio;
var log = console.log.bind(this);


module.exports.instantiateChatWithServer = function(server) {
  httpServer = server
  sio = require("socket.io")(server);
  sio.on('connect',connectedWithSocket);
}

var connectedWithSocket = function(socket) {
    socket.on('conversate', (conversation, callback) => {
      var room = String(conversation.itemId) + String(conversation.sellerId) + String(conversation.buyerId);
      socket.join(room);
      newConversation(conversation, room, callback);
    });

    socket.on('subscribe', (room) => {
       socket.join(room, function(data) {
         log(data);
       });
   });

    socket.on('leave conversation', (conversation) => {
      socket.leave(conversation);
    });

    socket.on('is typing', (room) => {
      // var room = {room:room.room}
      socket.to(room).emit("user typing", {typing:"true"});
    });

    socket.on('stopped typing', (room) => {
      // var room = {room:message.room}
      socket.to(room).emit("user typing", {typing:"false"});
    });

    socket.on('message', (message, callback) => {
      var emitMessage = function(socket,room,sqlMessage) {
        var room = {room:message.room}
        console.log(room);
        callback(sqlMessage.messageId);
        var broadcastMessage = {message:sqlMessage.message,
                                senderId: sqlMessage.senderId,
                                conversationId: sqlMessage.conversationId,
                                messageId: sqlMessage.messageId,
                                numberOfPictures: sqlMessage.numberOfPictures}
        socket.to(room).emit('received', broadcastMessage);
        getUserForIdAndSendNewMessagePush(message.recipientId, message.senderDisplayName, sqlMessage.message, sqlMessage.conversationId, sqlMessage.numberOfPictures);

      }
      newMessage(message, socket, emitMessage, callback);
    });

    socket.on('disconnect', () => {
      log("disconnected");
    });

    socket.on('error', (error) => {
    log(error);
  });
}

var newConversation = function(conversation, room, callback) {
  Conversation.create({
    itemId: conversation.itemId,
    sellerId: conversation.sellerId,
    buyerId: conversation.buyerId,
    room: room,
    lastMessage: conversation.message,
    sellerUnreadNumber: 1,
    buyerUnreadNumber: 0
  }).then(function(convo) {
    callback(convo.conversationId)
    newMessageWithNewConversation(conversation.message, convo, conversation)
  }).catch(function(error) {
    log(error)
  })
}

var newMessageWithNewConversation = function(message, conversation, socketMessage) {
  Message.create({
    conversationId: conversation.conversationId,
    senderId: conversation.buyerId,
    message: message,
    numberOfPictures: 0
  }).then(function(message) {
    getUserForIdAndSendNewConversationPush(socketMessage.sellerId,socketMessage.senderDisplayName, socketMessage.itemName, conversation.conversationId);
    log("success")
  }).catch(function(error) {
    log(error)
  })
}

var newMessage = function(message, socket, emitMessage, callback) {
  Message.create({
    conversationId: message.conversationId,
    senderId: message.senderId,
    message: message.message,
    numberOfPictures: message.numberOfPictures
  }).then(function(sqlMessage) {
    emitMessage(socket,message.room,sqlMessage);
    addLastMessage(sqlMessage)
  }).catch(function(error) {
    log(error)
  })
}

var getUserForIdAndSendNewConversationPush = function(userId, senderDisplayName, itemName, conversationId) {
  User.findOne({
      where: {
          'userId': userId
      }
  }).then(function(user) {
    var message = senderDisplayName + " has created a conversation about " + itemName
    PushAgent.sendPushWithAlertAndType(user.apnsToken, user.badgeCount+1, message, "newConversation" )
    incrementBadgeCountForUser(user, user.badgeCount+1)
  }).catch(function(error) {
    console.log(error);
  })
}

var getUserForIdAndSendNewMessagePush = function(userId, senderDisplayName, text, conversationId, numberOfPictures) {
  User.findOne({
      where: {
          'userId': userId
      }

  }).then(function(user) {
    var message;
    var type;
    if (numberOfPictures > 0) {
      message = senderDisplayName + " sent you an image!"
      type = "newMessageMedia"
    } else {
      message = senderDisplayName + ": " + text
      type = "newMessage"
    }
    PushAgent.sendPushWithAlertAndType(user.apnsToken, user.badgeCount+1, message, type , conversationId)
    incrementBadgeCountForUser(user, user.badgeCount+1)
  }).catch(function(error) {
    console.log(error);
  })
}

var incrementBadgeCountForUser = function(user, badgeCount) {
  user.updateAttributes({
    badgeCount: badgeCount
  }).catch(function(error) {
    console.log(error);
  })
}

var getItemForId = function(itemId) {
  Item.findOne({
    where: {
        'itemId': parseInt(itemId)
    }
  }).then(function(item) {
    return item
  }).catch(function(error) {
    console.log(error);
  })
}

var addLastMessage = function(message) {
  Conversation.findOne({
    where: {
      conversationId : message.conversationId
    }
  }).then(function(conversation) {
    var lastMessage = message.message
    if(message.numberOfPictures > 0) {
      lastMessage = "An Image Was Sent"
    } var sellerUnread = conversation.sellerUnreadNumber
    var buyerUnread = conversation.buyerUnreadNumber
    if(message.senderId == conversation.sellerId) {
      sellerUnread = sellerUnread + 1;
    } else {
      buyerUnread = buyerUnread + 1;
    }
    conversation.updateAttributes({
      lastMessage: message.message,
      sellerUnreadNumber: sellerUnread,
      buyerUnreadNumber: buyerUnread
    }).then(function() {
      console.log("Here");
    })
  })
}
