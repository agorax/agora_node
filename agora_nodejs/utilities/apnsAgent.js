var join = require('path').join
  , pfx = join(__dirname, '../certificates/agora-dev.p12');

// Create a new agent
var apnagent = require('apnagent')
  , agent = module.exports = new apnagent.Agent();

  // set our credentials
  agent.set('pfx file', pfx);
  agent.set("passphrase","kmk123")

  // our credentials were for development
  agent.enable('sandbox');

  agent.connect(function (err) {
    // gracefully handle auth problems
    if (err && err.name === 'GatewayAuthorizationError') {
      console.log('Authentication Error: %s', err.message);
      process.exit(1);
    }

    // handle any other err (not likely)
    else if (err) {
      throw err;
    }

    // it worked!
    var env = agent.enabled('sandbox')
      ? 'sandbox'
      : 'production';

    console.log('apnagent [%s] gateway connected', env);
  });

  var sendPushWithAlertAndType = function(deviceToken, badgeCount, message, type, conversationId) {
    agent.createMessage()
      .device(deviceToken)
      .alert(message)
      .badge(badgeCount)
      .set("type", type)
      .set("conversationId", conversationId)
      .send();
  }

  var sendItemPushWithAlertAndType = function(deviceToken, badgeCount, message, type, itemId) {
    agent.createMessage()
      .device(deviceToken)
      .alert(message)
      .set("type", type)
      .set("itemId", itemId)
      .send();
  }

  module.exports.sendPushWithAlertAndType = sendPushWithAlertAndType
  module.exports.sendItemPushWithAlertAndType = sendItemPushWithAlertAndType

  module.exports.agent = agent;
