var wrongPassword = {
    error: {
        name: 'Authentication Error.',
        message: 'Whoops, it looks like you have entered the wrong password!',
        statusCode: 501
    },
};

var noUserFound = {
    error: {
        name: 'Authentication Error.',
        message: 'Hey there! Looks like you haven\'t created an account yet! Create a new account now?',
        statusCode: 502
    }
};

var verifyEmail = {
    error: {
        name: 'Authentication Error.',
        message: 'Hey there! Looks like you need to verify your email still! Wait a few minutes for the email :)',
        statusCode: 503
    }
};

var internalError = {
    error: {
        name: 'Authentication Error.',
        message: 'Whoops, this one is our bad... Please try again in a couple minutes or contact support (jsw104@case.edu).',
        statusCode: 504
    }
};

var notSchoolEmail = function (school) {
    return {
        error: {
            name: 'Registration Error.',
            message: 'Please enter a valid ' + school + ' email',
            statusCode: 600
        }
    }
}

var passwordLength = {
    error: {
        name: 'Registration Error.',
        message: 'Sorry! All passwords must be at least 8 characters long.',
        statusCode: 601
    }
};

var userAlreadyExists = {
    error: {
        name: 'Registration Error.',
        message: 'Sorry, but it seems like a user already exists with this email!',
        statusCode: 602
    }
};

var noEmailFound = {
    error: {
        name: 'Registration Error.',
        message: 'Sorry, but it seems like we can\'t find your email!',
        statusCode: 603
    }
};

var noConversations = {
  error: {
    name: "Chat Error.",
    message: 'Sorry, we could not find any conversation for you!',
    statusCode: 701
  }
}

var noConversation = {
  error: {
    name: "Chat Error.",
    message: 'Sorry, we could not find that conversation!',
    statusCode: 702
  }
}

var noMessages = {
    error: {
        name: "Chat Error.",
        message: 'Sorry, we could not find the messages for this conversation!',
        statusCode: 703
    }
}

var conversationDeletion = {
    error: {
        name: "Chat Error.",
        message: 'Sorry, there was an error deleting this conversation',
        statusCode: 704
    }
}

var profileUpdate = {
    error: {
        name: "There was an error updating your profile.",
        message: 'Sorry, please try again later!',
        statusCode: 800
    }
}

var itemUpdate = {
    error: {
        name: "There was an error updating your item.",
        message: 'Sorry, please try again later!',
        statusCode: 801
    }
}

var addReview = {
    error: {
        name: "There was an error adding your review.",
        message: 'Sorry, please try again later!',
        statusCode: 802
    }
}

var itemDelete = {
    error: {
        name: "There was an error deleting your item.",
        message: 'Sorry, please try again later!',
        statusCode: 803
    }
}

module.exports.wrongPassword = wrongPassword;
module.exports.noUserFound = noUserFound;
module.exports.internalError = internalError;
module.exports.notSchoolEmail = notSchoolEmail;
module.exports.passwordLength = passwordLength;
module.exports.userAlreadyExists = userAlreadyExists;
module.exports.noEmailFound = noEmailFound;
module.exports.verifyEmail = verifyEmail;
module.exports.noConversations = noConversations;
module.exports.noConversation = noConversation;
module.exports.noMessages = noMessages;
module.exports.profileUpdate = profileUpdate;
module.exports.itemUpdate = itemUpdate;
module.exports.conversationDeletion = conversationDeletion;
module.exports.addReview = addReview;
module.exports.itemDelete = itemDelete;