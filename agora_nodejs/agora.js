var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var http    = require("http");
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var db = require('./db/db'); // get our db file
var Sequelize = require('sequelize');
var User   = require('./Models/user')(db.db,Sequelize);
var passport = require('passport');
var flash = require('connect-flash');
var emailConfigs = require('./configurations/emailConfiguration')
// ROUTE HANDLERS
let loginHandler = require("./routing_handlers/loginHandler");
let registrationHandler = require("./routing_handlers/registrationHandler");
let verificationHandler = require("./routing_handlers/verificationHandler");
let marketplaceReloadHandler = require("./routing_handlers/marketplaceReloadHandler");
let profileUpdateHandler = require("./routing_handlers/profileUpdateHandler");
let itemUpdateHandler = require("./routing_handlers/itemUpdateHandler");
let sellHandler = require("./routing_handlers/sellHandler");
let reviewHandler = require("./routing_handlers/addReviewHandler");
let chatHandler = require("./routing_handlers/chatHandler");
let socketIOManager = require("./utilities/socketIOManager");
let itemDeleteHandler = require("./routing_handlers/itemDeleteHandler");

app.use(passport.initialize())
app.use(passport.session())

app.set('agoraAPISecretKey', db.secret);
app.set('agoraEmailSecretKey', emailConfigs.secret);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var server = http.createServer(app).listen(8000, function () {
    console.log("Server running at port 8000.");
});
socketIOManager.instantiateChatWithServer(server);
// use morgan to log requests to the console
app.use(morgan('dev'));
db.client

var restrictedRoutes = express.Router();
var unrestrictedRoutes = express.Router();

unrestrictedRoutes.use('/login', loginHandler);

unrestrictedRoutes.use('/register', registrationHandler);

unrestrictedRoutes.use('/verify', verificationHandler);

restrictedRoutes.use(function(req, res, next) {
  console.log("apiroutes.use")
  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['token'];
  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, app.get('agoraAPISecretKey'), function(err, decoded) {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        next();
      }
    });

  } else {
    // if there is no token
    // return an error
    return res.status(403).send({
        success: false,
        message: 'No token provided.'
    });

  }
});

restrictedRoutes.use('/marketplaceReload', marketplaceReloadHandler);
restrictedRoutes.use('/sell', sellHandler);
restrictedRoutes.use('/chat', chatHandler);
restrictedRoutes.use('/profileUpdate', profileUpdateHandler);
restrictedRoutes.use('/itemUpdate', itemUpdateHandler);
restrictedRoutes.use('/review', reviewHandler);
restrictedRoutes.use('/itemDelete', itemDeleteHandler);

app.use('/api', restrictedRoutes);
app.use('/', unrestrictedRoutes);
