var nodemailer = require('nodemailer');

module.exports.transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'agoratechnologydevelopment@gmail.com', // Your email id
            pass: 'agorapassword' // Your password
        }
});

var ADMIN_EMAIL = 'EMAIL'
module.exports.ADMIN_EMAIL = ADMIN_EMAIL
module.exports.secret = "emailKey";

module.exports.EMAIL_OPTIONS = function(user,token) {
  var html = buildHtml(token)
  return {
      from: ADMIN_EMAIL, // sender address
      to: user.email, // list of receivers
      subject: 'Agora Account Verification', // Subject line
      text: "http://localhost:8000/verify?token=${token}" , // plaintext body
      html: html // You can choose to send an HTML body instead
  }
}

function buildHtml(token) {
    return `<table class="body-wrap">
	<tr>
		<td></td>
		<td class="container" width="600">
			<div class="content">
				<table class="main" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="content-wrap">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="content-block">
										Please confirm your email address by clicking the link below.
									</td>
								</tr>
								<tr>
									<td class="content-block">
										We may need to send you critical information about our service and it is important that we have an accurate email address.
									</td>
								</tr>
								<tr>
									<td class="content-block">
										<a href="http://localhost:8000/verify?token=${token}" class="btn-primary">Confirm email address</a>
									</td>
								</tr>
								<tr>
									<td class="content-block">
										&mdash; The Mailgunners
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div class="footer">
					<table width="100%">
						<tr>
							<td class="aligncenter content-block">Follow <a href="http://twitter.com/mail_gun">@Mail_Gun</a> on Twitter.</td>
						</tr>
					</table>
				</div></div>
		</td>
		<td></td>
	</tr>
</table>`
}
